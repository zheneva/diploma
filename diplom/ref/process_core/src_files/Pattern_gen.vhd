library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity Pattern_gen is
generic(
       mode : boolean := true;
       H_b_porch : integer ;
       V_b_porch : integer ;

       H_s_pulse : integer   ;     
       V_s_pulse : integer 
  );
        
  port (
     vidon : in std_logic;
     x_pos : in std_logic_vector(9 downto 0);               -- horizontal counter 
     y_pos : in std_logic_vector(9 downto 0);               -- vertical   counter
  r_chanel : out std_logic_vector(4 downto 0);
  g_chanel : out std_logic_vector(5 downto 0);
  b_chanel : out std_logic_vector(4 downto 0)
  ) ;
end Pattern_gen;
   
architecture Pattern_gen_test of Pattern_gen is


  -- signal flag_1 : std_logic;  
  -- signal flag_2 : std_logic;
  -- signal flag_3 : std_logic;
  -- signal r_chanel_tmp : unsigned(4 downto 0) := (others => '1');
  -- signal g_chanel_tmp : unsigned(5 downto 0) := (others => '0'); 
  -- signal b_chanel_tmp : unsigned(4 downto 0) := (others => '0');
  -- signal RGB_chanels_tmp : unsigned(15 downto 0) := (others => '0');

  -- signal inc_enable   : std_logic := '0';

  signal x_pos_tmp :  std_logic_vector(9 downto 0);
  signal y_pos_tmp :  std_logic_vector(9 downto 0);


begin


x_pos_tmp <= x_pos - H_b_porch - H_s_pulse;
y_pos_tmp <= y_pos - V_b_porch - V_s_pulse;

Chess_board: if (mode) generate

    process(x_pos_tmp, y_pos_tmp, vidon)                                             -- Chess board
    begin 
      if (vidon = '1') then 
          if (y_pos_tmp(1) = '1') then
              r_chanel <= (others => x_pos_tmp(1));
              b_chanel <= (others => not x_pos_tmp(1));
          else
              r_chanel <= (others => not x_pos_tmp(1));
              b_chanel <= (others => x_pos_tmp(1));
          end if;
      else 
          r_chanel <= (others => '0');
          b_chanel <= (others => '0');
      end if;
    end process;
    
    g_chanel <= (others => '0');
end generate Chess_board;

pixel_frame: if (not mode) generate

process(x_pos_tmp, y_pos_tmp)                                            -- Chess board
  begin 
    if (x_pos_tmp = 0 or y_pos_tmp = 0 or x_pos_tmp = 9 or y_pos_tmp = 479) then  -- visible area(480 or 640) - 1 due to 
     r_chanel <= (others => '1');                                                              -- we start count from 0 -> 0 to 639 = 640px
     g_chanel <= (others => '1');
     b_chanel <= (others => '1');
   else
     r_chanel <= (others => '0');
     g_chanel <= (others => '0');
     b_chanel <= (others => '0');
   end if;
end process;
end generate pixel_frame;
end Pattern_gen_test ; -- Pattern_gen_test