library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sum is
    port (
        Data_1 : in std_logic_vector(9 downto 0);
        Data_2 : in std_logic_vector(9 downto 0);
        Data_3 : in std_logic_vector(9 downto 0);
        Data_4 : in std_logic_vector(9 downto 0);
        Data_5 : in std_logic_vector(9 downto 0);
        Data_6 : in std_logic_vector(9 downto 0);
        Data_7 : in std_logic_vector(9 downto 0);
        Data_8 : in std_logic_vector(9 downto 0);
        Data_9 : in std_logic_vector(9 downto 0);
        sum_out : out std_logic_vector(9 downto 0)
    );
end sum;

architecture sum_struct of sum is



begin

sum_out <= std_logic_vector( signed(Data_1) + signed(Data_2) + signed(Data_3) + 
                                    signed(Data_4) + signed(Data_5) + signed(Data_6) + 
                                        signed(Data_7) + signed(Data_8) + signed(Data_9));

end sum_struct;