library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity core_gen is
  generic(
    --mode : boolean := true;
    H_pixel   : integer :=  640; -- //? 640; 
    V_lines   : integer :=  480;-- //? 480;
    H_f_porch : integer :=  16;  -- //? 16 ; 
    H_b_porch : integer :=  48;  -- //? 16 ; 
    H_s_pulse : integer :=  96;  -- //? 128;
    V_f_porch : integer :=  10; -- //? 10 ; 
    V_b_porch : integer :=  29; -- //? 29 ;
    V_s_pulse : integer :=  2  -- //? 2
    );
  port (
    clk : in std_logic;
    clr : in std_logic;
    ch_cernel  : in std_logic;
    vga_hs   : out std_logic;
    vga_vs   : out std_logic;
    vga_r : out std_logic_vector(4 downto 0);
    vga_g : out std_logic_vector(5 downto 0);
    vga_b : out std_logic_vector(4 downto 0)

  );
end core_gen;

architecture core_gen_struct of core_gen is

component CORE 
  port (
      clk  : in std_logic;
      w_en : in std_logic;
    cnv_en : in std_logic;
    ch_cernel : in std_logic;
    inDATA : in std_logic_vector(4 downto 0);
     shift : in natural;
   coreOUT : out std_logic_vector(4 downto 0)
  );
end component;

component Pattern_gen 

  generic(
  mode : boolean := true;
  H_b_porch : integer := H_b_porch ;
  V_b_porch : integer := V_b_porch ;

  H_s_pulse : integer := H_s_pulse ;
  V_s_pulse : integer := V_s_pulse
  );
 
  port (
     vidon : in std_logic;
     x_pos : in std_logic_vector(9 downto 0);               -- horizontal counter 
     y_pos : in std_logic_vector(9 downto 0);               -- vertical   counter
  r_chanel : out std_logic_vector(4 downto 0);
  g_chanel : out std_logic_vector(5 downto 0);
  b_chanel : out std_logic_vector(4 downto 0)
  ) ;
end component;

signal hc, vc : std_logic_vector(9 downto 0) := (others => '0'); 
signal vidon  : std_logic; 
signal vc_enable : std_logic; 

signal w_en   : std_logic; 
signal cnv_en : std_logic := '0'; 
signal shift  : natural := 4;

signal r_chanel_pg : std_logic_vector(4 downto 0);
signal g_chanel_pg : std_logic_vector(5 downto 0);
signal b_chanel_pg : std_logic_vector(4 downto 0);

signal core_gen_r_out :  std_logic_vector(4 downto 0) ;
signal core_gen_g_out :  std_logic_vector(5 downto 0) ;
signal core_gen_b_out :  std_logic_vector(4 downto 0) ;

begin

process(clk, clr)
  begin

  if (rising_edge(clk)) then 
    if (clr = '1') then
      hc <= (others => '0');
    else
      if hc = std_logic_vector(to_unsigned((H_f_porch + H_b_porch + H_s_pulse + H_pixel - 1), hc'length)) then   -- hc = 799
        hc <= (others => '0') ;
        vc_enable <= '1';
      else 
        hc <= std_logic_vector( unsigned(hc) + 1);
        vc_enable <= '0';
      end if;
    end if;
  end if;
end process;  

vga_hs <= '0' when (hc < std_logic_vector(to_unsigned((H_s_pulse) + 3, vc'length)) and
                        hc > std_logic_vector(to_unsigned(3, vc'length))) else '1';  

process(clk, clr)
 begin
  if (rising_edge(clk)) then 
    if (clr = '1') then 
      vc <= (others => '0');
    elsif (vc_enable = '1') then 
      if(vc = std_logic_vector(to_unsigned((V_f_porch + V_b_porch + V_s_pulse + V_lines - 1), vc'length))) then -- vc = 520
        vc <= (others => '0');
      else
        vc <= std_logic_vector( unsigned(vc) + 1);
      end if;
    else 
      vc <= vc;
    end if;
  end if;
end process;

vga_vs <= '0' when (vc < std_logic_vector(to_unsigned((V_s_pulse + 3), vc'length)) and 
                        vc > std_logic_vector(to_unsigned(3, vc'length))) else '1';  


vidon <= '1' when (((hc > std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch - 1), hc'length))) and                   --  144 < hc <  784  --- проверить диапазоны, как будто не хвататет одного пикселя 784-144 = 640
                      (hc < std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch + H_pixel), hc'length)))) and          --- но мы не включаем граничные значения поэтому фактиеческая длина окна - 784-144-1
                        ((vc < std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch + V_lines), vc'length))) and        -- 30 < vc < 511 
                           (vc > std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch - 1), vc'length))))) else '0'
;

w_en  <= '1' when (((hc > std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch - 2), hc'length))) and                   --//! добвдены доб значения для формирования zero-padding
                      (hc < std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch + H_pixel + 1), hc'length)))) and      --//! +2 т.к. после прихода последней (заполненой нулями) строки нужно продожать запись как минимум 3 такта чтобы заполнились регистры
                        ((vc < std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch + V_lines + 2), vc'length))) and    --//! проще записать в буфер еще одну строку, которая потом все равно перезапишеться на стадии инициализации буфера перед след кадром
                           (vc > std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch - 2), vc'length))))) else '0'
;



cnv_en  <= '1' when (((hc > std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch + 3 - 2), hc'length))) and                   --//! взять нормальные данные (универ) и добавить по одному отсчету сверху и снизу 
                      (hc < std_logic_vector(to_unsigned(( H_s_pulse + H_b_porch + H_pixel + 2), hc'length)))) and          --//! для формирования zero-padding
                        ((vc < std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch + V_lines + 2), vc'length))) and         --? 2 - задержка выхода (2 такта длится свертка -> выход смещен)
                           (vc > std_logic_vector(to_unsigned(( V_s_pulse + V_b_porch + 3 - 2), vc'length))))) else '0'      --? 3 - размер фильтра
;

circ_pg: Pattern_gen 
      port map(
        vidon => vidon,
        x_pos => hc,               -- horizontal counter 
        y_pos => vc,               -- vertical   counter
    r_chanel => r_chanel_pg,
    g_chanel => g_chanel_pg, --vga_g,
    b_chanel => b_chanel_pg  --vga_b 
) ;


circ_core_red: CORE 
  port map(
       clk => clk,  
      w_en => w_en,  
    cnv_en => cnv_en,
    ch_cernel => ch_cernel,  
    inDATA => r_chanel_pg,  
     shift => shift, 
   coreOUT => core_gen_r_out   
);

circ_blue_red: CORE 
  port map(
       clk => clk,  
      w_en => w_en,  
    cnv_en => cnv_en,
    ch_cernel => ch_cernel,  
    inDATA => b_chanel_pg,  
     shift => shift, 
   coreOUT => core_gen_b_out   
);
circ_gren_red: CORE 
  port map(
       clk => clk,  
      w_en => w_en,  
    cnv_en => cnv_en,
    ch_cernel => ch_cernel,  
    inDATA => g_chanel_pg(4 downto 0),  
     shift => shift, 
   coreOUT => core_gen_g_out(4 downto 0)   
);

process(clk)
 begin
  if (rising_edge(clk)) then
    vga_r <= core_gen_r_out;
    vga_b <= core_gen_b_out;   
    vga_g <= core_gen_g_out;     
  end if;
end process;
end core_gen_struct ; -- core_gen_struct




 
--! УБРАТЬ ПР�?СВОЕН�?Е "X" НА CNV 
--ToDoo пофиксить разрядности
--* переписать сигналы vidon и w_en с новыми vga
--* добавить сигнал cnv_en - будет определять операцию свертки (пропуск 3 строк каждого кадра(заполнение) 3 пикселей каждой строки) 
--* пропуск 3 строк каждого кадра(заполнение буфера) и 3 пикселей каждой строки(заполнение регистров)
--*- поправить w_en  сдвинуть на такт спереди и сзади
--*- Поправить разрядности 4 бита на канал вместо 8-ми
--!- Проверить след кадр
--?- Посмотреть тайминги выходного потока (прикрутить vga выход)
--?- запустить синтез хотя бы на рабочем компе